# wintersport2022

Public docs & links re: wintersport in Oostenrijk

De stricte regels rondom toegang tot Oostenrijk zijn te komen vervallen. Zie de links hieronder welke regels er nu gelden.

## Handige links

De eerste link (naar Summit Travel), beantwoord, denk ik, de meeste vragen en rare uitzonderingen. Ook wordt deze pagina relatief vaak geupdate. Kijk hier dus voor veelgestelde vragen, en mogelijk de antwoorden op die vragen.

- [Coronapagina van Summit Travel](https://www.summittravel.nl/Wintersport/Ski/Coronamaatregelen-Oostenrijk)
- [Corona Rules Austria (English)](https://www.austria.info/en/service-and-facts/coronavirus-information/entry-regulations)
- [Regelgeving Oostenrijk (Nederlands)](https://www.austria.info/nl/praktische-info/coronavirus-informatie)
- Regels en maatregelen in Maria Alm zelf [FAQ](https://www.skiamade.com/corona_en), ook hoe de ski-pas te koppelen aan je corona-id

## Checklist
- Zorg voor een reisverzekering met wintersportdekking [bijvoorbeeld bij ANWB](https://www.anwb.nl/verzekeringen/reisverzekering/wintersportverzekering)

Last update:24-01-2022 10:40
